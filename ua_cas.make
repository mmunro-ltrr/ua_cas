api = 2
core = 7.x

; CAS contrib module.
projects[cas][version] = 1.6
projects[cas][subdir] = contrib

; phpCAS library.
libraries[CAS][download][type] = get
libraries[CAS][download][url] = https://github.com/Jasig/phpCAS/archive/1.3.5.tar.gz
